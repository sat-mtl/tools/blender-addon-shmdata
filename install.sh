#!/bin/bash
version=`blender --version | egrep -o "Blender [0-9.]+" | egrep -o "[0-9.]+"`
mkdir -p ${HOME}/.config/blender/${version}/scripts/addons/
ln -s $(pwd)/shmdata ${HOME}/.config/blender/${version}/scripts/addons/
