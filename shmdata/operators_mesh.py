#
# Copyright (C) 2015 Emmanuel Durand
#
# This file is part of Splash (http://github.com/paperManu/splash)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Splash is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Splash.  If not, see <http://www.gnu.org/licenses/>.
#

import bpy
import bmesh
import struct
import time
import sys

from bpy.types import Operator
from dataclasses import dataclass
from mathutils import Vector
from typing import Dict, Optional

sys.path.insert(0, '/usr/local/lib/python3/dist-packages')

try:
    from pyshmdata import Writer
except ModuleNotFoundError as ex:
    print(f"Could not load pyshmdata: {ex}")


@dataclass
class Target:
    object_name: str
    writer: Optional[Writer] = None
    start_time: float = 0.0


class ShmdataMesh:
    targets: Dict[str, Target] = {}

    @classmethod
    def add_target(cls, target: Target) -> None:
        if target.object_name not in ShmdataMesh.targets:
            ShmdataMesh.targets[target.object_name] = target

    @classmethod
    def remove_target(cls, object_name: str) -> None:
        if object_name in ShmdataMesh.targets.keys():
            target = ShmdataMesh.targets[object_name]
            ShmdataMesh.targets.pop(target.object_name)

    @classmethod
    def send_mesh_callback(cls, scene: bpy.types.Scene, depsgraph: bpy.types.Depsgraph) -> None:
        for name, target in ShmdataMesh.targets.items():
            ShmdataMesh.send_mesh(target=target, scene=scene)

    @classmethod
    def send_mesh(cls, target: Target, scene: bpy.types.Scene) -> None:
        context = bpy.context

        obj = bpy.data.objects[target.object_name]

        world_matrix = obj.matrix_world

        normal_matrix = world_matrix.copy()
        normal_matrix.invert()
        normal_matrix.transpose()

        if context.edit_object is not None:
            mesh = bmesh.from_edit_mesh(obj.data)
            buffer_vert = bytearray()
            buffer_poly = bytearray()
            buffer_data = bytearray()

            vert_count = 0
            poly_count = 0

            uv_layer = mesh.loops.layers.uv.active
            if uv_layer is None:
                bpy.ops.uv.smart_project()
                uv_layer = mesh.loops.layers.uv.active

            for face in mesh.faces:
                poly_count += 1
                buffer_poly += struct.pack("i", len(face.verts))
                for loop in face.loops:
                    buffer_poly += struct.pack("i", vert_count)

                    v = loop.vert.co
                    tmp_vector = Vector([*v, 1.0])
                    tmp_vector = world_matrix @ tmp_vector
                    v = Vector(tmp_vector[0:3])

                    n = loop.vert.normal
                    tmp_vector = Vector([*n, 0.0])
                    tmp_vector = normal_matrix @ tmp_vector
                    n = Vector(tmp_vector[0:3])

                    if uv_layer is None:
                        uv = Vector([0.0, 0.0])
                    else:
                        uv = loop[uv_layer].uv

                    buffer_vert += struct.pack("ffffffff", *v, *uv, *n)
                    vert_count += 1

            buffer_data += struct.pack("ii", vert_count, poly_count)
            buffer_data += buffer_vert
            buffer_data += buffer_poly

            target.writer.push(buffer_data)
        else:
            if type(obj.data) is bpy.types.Mesh:
                if not obj.data.uv_layers:
                    bpy.ops.object.editmode_toggle()
                    bpy.ops.uv.smart_project()
                    bpy.ops.object.editmode_toggle()

                mesh = obj.to_mesh()
                buffer_vert = bytearray()
                buffer_poly = bytearray()
                buffer_data = bytearray()

                vert_count = 0
                poly_count = 0

                for poly in mesh.polygons:
                    poly_count += 1
                    buffer_poly += struct.pack("i", len(poly.loop_indices))
                    for index in poly.loop_indices:
                        buffer_poly += struct.pack("i", vert_count)

                        v = mesh.vertices[mesh.loops[index].vertex_index].co
                        tmp_vector = Vector([*v, 1.0])
                        tmp_vector = world_matrix @ tmp_vector
                        v = Vector(tmp_vector[0:3])

                        n = mesh.vertices[mesh.loops[index].vertex_index].normal
                        tmp_vector = Vector([*n, 0.0])
                        tmp_vector = normal_matrix @ tmp_vector
                        n = Vector(tmp_vector[0:3])

                        if mesh.uv_layers:
                            uv = mesh.uv_layers[0].data[index].uv
                        else:
                            uv = Vector([0.0, 0.0])

                        buffer_vert += struct.pack("ffffffff", *v, *uv, *n)
                        vert_count += 1

                buffer_data += struct.pack("ii", vert_count, poly_count)
                buffer_data += buffer_vert
                buffer_data += buffer_poly

                target.writer.push(buffer_data)


class ShmdataMeshExport(Operator):
    """
    Activate the output of the selected mesh to shmdata
    """
    bl_idname = "shmdata.send_mesh_to_shmdata"
    bl_label = "Activate the output of the selected mesh to shmdata"

    def execute(self, context: bpy.types.Context):
        path_prefix = "/tmp/blender_shmdata_mesh_"
        object_name = context.active_object.name
        path = path_prefix + context.active_object.name

        if context.active_object.shmdata_mesh_active:
            context.active_object.shmdata_mesh_active = False
            ShmdataMesh.remove_target(object_name)
        else:
            context.active_object.shmdata_mesh_active = True

            writer = Writer(path=path, datatype="application/x-polymesh")
            target = Target(object_name=object_name,
                            writer=writer,
                            start_time=time.clock_gettime(time.CLOCK_REALTIME))

            ShmdataMesh.add_target(target)
            if ShmdataMesh.send_mesh not in bpy.app.handlers.depsgraph_update_post:
                bpy.app.handlers.depsgraph_update_post.append(ShmdataMesh.send_mesh_callback)

        return {'FINISHED'}
